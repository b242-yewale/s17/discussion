console.log("Hello Onkar!");

// [Section] Functions

/*
	Functions in JavaScript are lines/blocks of code that tell our device to perform a certain task when they
	are called/invoked.

	Syntax:
	function functionName(){
		codes to be executed (statements);
	}

*/

function printName(){
	console.log("Hello Again:)");
}

printName();

printName();

// In JS, functions can be hoisted. That means functions can be used in the flow of code before they are created.

declaredFunction();

function declaredFunction(){
	console.log("Hello from declaredFunction()");
}

declaredFunction();

// [Section] Function Expression
// a function can be stored in a variable.
// a function expression is an anonymous function assigned to a variable.

/*
	Syntax:
	let/const variableFunctionName = function(){
		codes to be executed/statements
	}
*/

// variableFunction();

/*
	error - function expressions, being let/const variables that have function values, cannot be hoisted in JS
*/

let variableFunction = function(){
	console.log("Hello from variableFunction()");
}

variableFunction();


let funcExpression = function funcName(){
	console.log("Hello from the other side!");
}

// funcName();
funcExpression();

// reassign declared functions and function expression to new anonymous functions

declaredFunction = function(){
	console.log("updated declaredFunction");
}
declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression()");
}

funcExpression();

// we cannot reassign a new function expression initialised with const keyword

const constFunction = function(){
	console.log("Const Function");
}
constFunction();

/*constFunction = function(){
	console.log("connot be reassigned!");
}
constFunction();*/

// [Section] Function Scoping
/*
	Scope is the accesibility / visibility of the variables

	JS has 3 scopes for its variables
		1. Local/ Block scope
		2. Global scope
		3. Function Scope
			JS has function scope: each function creates a new scope
			variables defined inside a function are not accessible from outside the function.
*/
let globalVar = "Aarush Yewale";

{
	let localVar = "Onkar Yewale";
	console.log(localVar);
	console.log(globalVar);
}

console.log(globalVar);
// console.log(localVar);  Returns an error as localVar can only be accessed inside {} where it is created

function showNames(){
	const functionConst = "Onkar";
	let functionLet = "Aarush";

	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// returns an error because they are inside the function showNames()
/*console.log(functionConst);
console.log(functionLet);*/

// Nested Functions
function myNewFunction(){
	let name = "Aarush";

	function nestedFunction(){
		let nestedVar = "Onkar";
		console.log(name); // will work because nestedFunction() is still inside the function (myNewFunction) where "name" variable is declared		
	}
	// console.log(nestedVar);  results in error because of scoping in JS
	nestedFunction();  //works because nestedFunction() is called inside a function where it is declared
}
myNewFunction();
// nestedFunction();	results in error as nestedFunction() is called from outside the function in which it is declared


let globalName = "Aarush";
function newFunction2(){
	let nameInside = "Onkar";
	console.log(nameInside);
	console.log(globalName);
}

newFunction2();
// console.log(nameInside);

// [Section] Use of alert()

// alert("Hello Aarush!");
// we can also use alert() to show a message to the users from a later function invocation

function sampleAlert(){
	alert("Hello, Mayu!");
}
// sampleAlert();

console.log("I will only log in the console after the alert is closed.");

// [Section] Use of Prompt()
// prompt() allows us to show a small window at the top of the browser page to gather user input. 
// Much like alert, it will have the page wait until the user gives an input.
// input from the prompt() will be returned as a string data type once the user dismisses the window.

/*let samplePrompt = prompt("Enter your name: ");
console.log("Hello, "+samplePrompt);
console.log(typeof samplePrompt); */    //returns a string data type if an input is given. 
// if no input is given anwe click cancel, then it returns an object.
// returns an empty string is no input is given and we click ok.

function miniActivity(){
	let firstName = prompt("Enter first name: ");
	let lastName = prompt("Enter last name: ");
	console.log("Hello "+firstName +" "+lastName+"! Welcome to my page!");
}

miniActivity();




